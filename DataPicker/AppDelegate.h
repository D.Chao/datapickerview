//
//  AppDelegate.h
//  DataPicker
//
//  Created by 趙子超 on 2019/11/15.
//  Copyright © 2019 chao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

