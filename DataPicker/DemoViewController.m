//
//  DemoViewController.m
//  DataPicker
//
//  Created by 趙子超 on 2019/11/15.
//  Copyright © 2019 chao. All rights reserved.
//

#import "DemoViewController.h"
#import "DataPickerView.h"

@interface DemoViewController ()<DataPickerViewDataSource, DataPickerViewDelegate>

@property (nonatomic, strong) DataPickerView *dataPickerView;

@property (nonatomic, strong) NSArray *dataArray;

@end

@implementation DemoViewController

#pragma mark - Life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataArray = @[@"One", @"Two", @"Three"];
    self.dataPickerView = [[DataPickerView alloc] init];
    self.dataPickerView.dataSource = self;
    self.dataPickerView.delegate = self;
}

#pragma mark - DataPickerViewDataSource
- (NSInteger)numberOfComponentsInDataPickerView:(DataPickerView *)dataPickerView {
    return 1;
}

- (NSInteger)dataPickerView:(DataPickerView *)dataPickerView numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

#pragma mark - DataPickerViewDelegate
- (NSString *)dataPickerView:(DataPickerView *)dataPickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.dataArray objectAtIndex:row];
}


#pragma mark - Layouts

#pragma mark - Private methods

#pragma mark - Actions
- (IBAction)buttonPressed:(UIButton *)sender {
    
    [self.dataPickerView show];
}

@end
