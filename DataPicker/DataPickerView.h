//
//  DataPickerView.h
//  DataPicker
//
//  Created by 趙子超 on 2019/11/15.
//  Copyright © 2019 chao. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class DataPickerView;

@protocol DataPickerViewDataSource <NSObject>

- (NSInteger)numberOfComponentsInDataPickerView:(DataPickerView *)dataPickerView;

- (NSInteger)dataPickerView:(DataPickerView *)dataPickerView numberOfRowsInComponent:(NSInteger)component;

@end

@protocol DataPickerViewDelegate <NSObject>

- (NSString *)dataPickerView:(DataPickerView *)dataPickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;

@optional

- (void)dataPickerView:(DataPickerView *)dataPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;

- (void)confirmButtonDidPressRow:(NSInteger)row component:(NSInteger)component;

- (void)cancelButtonDidPress;

@end



@interface DataPickerView : UIView

@property (nonatomic, weak) id<DataPickerViewDataSource> dataSource;

@property (nonatomic, weak) id<DataPickerViewDelegate> delegate;

- (instancetype)init;

- (void)setConfirmButtonTitle:(NSString *)confirmTitle;

- (void)setCancelButtonTitle:(NSString *)cancelTitle;

- (void)show;

- (void)reloadComponent:(NSInteger)component;

@end

NS_ASSUME_NONNULL_END
