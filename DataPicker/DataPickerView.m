//
//  DataPickerView.m
//  DataPicker
//
//  Created by 趙子超 on 2019/11/15.
//  Copyright © 2019 chao. All rights reserved.
//

#import "DataPickerView.h"

@interface DataPickerView ()<UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIView *pickerContentView;

@property (nonatomic, strong) UIPickerView *pickerView;

@property (nonatomic, strong) UIView *toolBarView;

@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, strong) UIButton *cancelButton;

@property (nonatomic, assign) NSInteger component;

@property (nonatomic, assign) NSInteger row;

@property (nonatomic, assign) CGRect screenRect;

@property (nonatomic, strong) UIView *lastView;

@end

@implementation DataPickerView

#pragma mark - Initialization
- (instancetype)init {
    
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self setupView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setupView];
    }
    return self;
}

#pragma mark - Layouts
- (void)setupView {
    
    self.backgroundColor = [UIColor clearColor];
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    _contentView = [[UIView alloc] init];
    [_contentView setTranslatesAutoresizingMaskIntoConstraints:NO];
    _contentView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [self addSubview:_contentView];
    [[_contentView.topAnchor constraintEqualToAnchor:self.topAnchor] setActive:YES];
    [[_contentView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor] setActive:YES];
    [[_contentView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor] setActive:YES];
    [[_contentView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor] setActive:YES];
    
    _pickerContentView = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.screenRect.size.height, self.screenRect.size.width, 300.0)];
    _pickerContentView.backgroundColor = [UIColor whiteColor];
    [_contentView addSubview:_pickerContentView];
    
    _toolBarView = [[UIView alloc] init];
    _toolBarView.backgroundColor = [UIColor colorWithWhite:240.0/255.0 alpha:1.0];
    [_toolBarView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_pickerContentView addSubview:_toolBarView];
    [[_toolBarView.topAnchor constraintEqualToAnchor:_pickerContentView.topAnchor] setActive:YES];
    [[_toolBarView.leadingAnchor constraintEqualToAnchor:_pickerContentView.leadingAnchor] setActive:YES];
    [[_toolBarView.trailingAnchor constraintEqualToAnchor:_pickerContentView.trailingAnchor] setActive:YES];
    [[_toolBarView.heightAnchor constraintEqualToConstant:55.0] setActive:YES];
    [_toolBarView addSubview:self.confirmButton];
    [[self.confirmButton.trailingAnchor constraintEqualToAnchor:_toolBarView.trailingAnchor constant:-15.0] setActive:YES];
    [[self.confirmButton.topAnchor constraintEqualToAnchor:_toolBarView.topAnchor constant:0.0] setActive:YES];
    [[self.confirmButton.bottomAnchor constraintEqualToAnchor:_toolBarView.bottomAnchor constant:0.0] setActive:YES];
    [[self.confirmButton.widthAnchor constraintEqualToConstant:60.0] setActive:YES];
    [_toolBarView addSubview:self.cancelButton];
    [[self.cancelButton.leadingAnchor constraintEqualToAnchor:_toolBarView.leadingAnchor constant:15.0] setActive:YES];
    [[self.cancelButton.topAnchor constraintEqualToAnchor:_toolBarView.topAnchor constant:0.0] setActive:YES];
    [[self.cancelButton.bottomAnchor constraintEqualToAnchor:_toolBarView.bottomAnchor constant:0.0] setActive:YES];
    [[self.cancelButton.widthAnchor constraintEqualToConstant:60.0] setActive:YES];
    
    _pickerView = [[UIPickerView alloc] init];
    _pickerView.dataSource = self;
    _pickerView.delegate = self;
    [_pickerView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_pickerContentView addSubview:_pickerView];
    [[_pickerView.topAnchor constraintEqualToAnchor:_toolBarView.bottomAnchor] setActive:YES];
    [[_pickerView.bottomAnchor constraintEqualToAnchor:_pickerContentView.bottomAnchor] setActive:YES];
    [[_pickerView.leadingAnchor constraintEqualToAnchor:_pickerContentView.leadingAnchor] setActive:YES];
    [[_pickerView.trailingAnchor constraintEqualToAnchor:_pickerContentView.trailingAnchor] setActive:YES];
}

#pragma mark - Property
- (UIButton *)confirmButton {
    
    if (!_confirmButton) {
        _confirmButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [_confirmButton setTitle:@"OK" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_confirmButton addTarget:self action:@selector(confirmButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightRegular];
        [_confirmButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    return _confirmButton;
}

- (UIButton *)cancelButton {
    
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(confirmButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightRegular];
        [_cancelButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
    return _cancelButton;
}

- (CGRect)screenRect {
    return [[UIScreen mainScreen] bounds];
}

- (UIView *)lastView {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    return keyWindow.subviews.lastObject;
}

#pragma mark - Public methods
- (void)show {
    
    [self.lastView addSubview:self];
    [[self.topAnchor constraintEqualToAnchor:self.lastView.topAnchor] setActive:YES];
    [[self.bottomAnchor constraintEqualToAnchor:self.lastView.bottomAnchor] setActive:YES];
    [[self.leadingAnchor constraintEqualToAnchor:self.lastView.leadingAnchor] setActive:YES];
    [[self.trailingAnchor constraintEqualToAnchor:self.lastView.trailingAnchor] setActive:YES];
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
       
        self.pickerContentView.frame = CGRectMake(0.0, self.screenRect.size.height - self.pickerContentView.bounds.size.height, self.screenRect.size.width, self.pickerContentView.bounds.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)reloadComponent:(NSInteger)component {
    [self.pickerView reloadComponent:component];
}

- (void)setConfirmButtonTitle:(NSString *)confirmTitle {
    [self.confirmButton setTitle:confirmTitle forState:UIControlStateNormal];
}

- (void)setCancelButtonTitle:(NSString *)cancelTitle {
    [self.cancelButton setTitle:cancelTitle forState:UIControlStateNormal];
}

#pragma mark - Private methods

#pragma mark - Actions
- (void)confirmButtonPressed:(UIButton *)sender {
    
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(DataPickerViewDelegate)] && [self.delegate respondsToSelector:@selector(confirmButtonDidPressRow:component:)]) {
        [self.delegate confirmButtonDidPressRow:self.row component:self.component];
    }
    [self dismiss];
}

- (void)cancelButtonPressed:(UIButton *)sender {
    
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(DataPickerViewDelegate)] && [self.delegate respondsToSelector:@selector(cancelButtonDidPress)]) {
        [self.delegate cancelButtonDidPress];
    }
    [self dismiss];
}

- (void)dismiss {
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.pickerContentView.frame = CGRectMake(0.0, self.screenRect.size.height, self.screenRect.size.width, self.pickerContentView.bounds.size.height);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    if (self.dataSource) {
        return [self.dataSource numberOfComponentsInDataPickerView:self];
    }
    return 0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (self.dataSource) {
        return [self.dataSource dataPickerView:self numberOfRowsInComponent:component];
    }
    return 0;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (self.dataSource) {
        return [self.delegate dataPickerView:self titleForRow:row forComponent:component];
    }
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.component = component;
    self.row = row;
    
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(DataPickerViewDelegate)] && [self.delegate respondsToSelector:@selector(dataPickerView:didSelectRow:inComponent:)]) {
        [self.delegate dataPickerView:self didSelectRow:row inComponent:component];
    }
}

@end
